﻿#define TEST_TIME

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace VioletaSchedules.NetCore
{
    public sealed class Scheduler
    {
        private struct RunTaskState
        {
            private readonly int _SleepTimeMiliseconds;
            public int TotalMiliseconds => _SleepTimeMiliseconds;

            public bool IsWatingForNext => _SleepTimeMiliseconds < 0;

            private RunTaskState(int sleepTimeMiliseconds)
            {
                _SleepTimeMiliseconds = sleepTimeMiliseconds;
            }

            public static RunTaskState WaitForNext() => new RunTaskState(-1);

            public static RunTaskState ToSleep(TimeSpan sleepTime)
                => new RunTaskState((int)sleepTime.TotalMilliseconds);
        }

        private sealed class TaskRunner
        {
            private Scheduler _ParentScheduler;
            private readonly IExecutionItem _ExecutionItem;

            private DateTime _LastFinishedTime;
            public DateTime LastFinishedTime => _LastFinishedTime;

            private Task _CurrentTask;
            private Action _OnCompleted;

            public TaskRunner(IExecutionItem executionItem, Scheduler parentScheduler)
            {
                _ExecutionItem = executionItem;
                _ParentScheduler = parentScheduler;
                _OnCompleted = OnCompleted;
            }

            public void Run()
            {
                Task currentTask;
                bool lockTaken = false;
                Monitor.TryEnter(this, ref lockTaken);
                if (!lockTaken) throw new InvalidOperationException();

                try
                {
                    if (_CurrentTask != null) throw new InvalidOperationException();
                    currentTask = _ExecutionItem.Run();
                    _CurrentTask = currentTask;
                }
                finally
                {
                    if (lockTaken) Monitor.Exit(this);
                }

                if (currentTask.IsCompleted)
                {
                    OnCompleted();
                }
                else
                {
                    var awaiter = currentTask.GetAwaiter();
                    awaiter.OnCompleted(_OnCompleted);
                }
            }

            private void OnCompleted()
            {
                Task currentTask;
                bool lockTaken = false;
                Monitor.TryEnter(this, ref lockTaken);
                if (!lockTaken) throw new InvalidOperationException();

                try
                {
                    currentTask = _CurrentTask;
                    if (currentTask == null) throw new InvalidOperationException();
                    _CurrentTask = null;

                }
                finally
                {
                    if (lockTaken) Monitor.Exit(this);
                }

                try
                {
                    if (!currentTask.IsCompleted) throw new InvalidOperationException();

                    if (currentTask.IsCanceled)
                        OnCancelled(currentTask);
                    else if (currentTask.IsFaulted)
                        OnFaulted(currentTask);
                    else
                        OnSuccessfully(currentTask);
                }
                finally
                {
                    currentTask.Dispose();
                }
            }

            private void OnCancelled(Task task) => throw new NotImplementedException();
            private void OnFaulted(Task task) => throw new NotImplementedException();
            private void OnSuccessfully(Task task) => _LastFinishedTime = _ParentScheduler.OnFinished(this);
        }

        private readonly ReaderWriterLockSlim _UpdateQueue = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

        private readonly ManualResetEventSlim _RunnerMRE = new ManualResetEventSlim(false);
        private readonly Thread _TaskRunner;

        public TimeSpan SleepTime { get; }


        private int _Shift = 10;
        private long _Mask = ((0x1 << 10) - 1) | long.MinValue;
        private TaskRunner[] _Queue = new TaskRunner[0x1 << 10];

        private int _Count;
        private long _Index = -1;

        public Scheduler(TimeSpan sleepTime)
        {
            if (sleepTime < TimeSpan.FromSeconds(0.1)) throw new ArgumentOutOfRangeException(nameof(sleepTime));
            SleepTime = sleepTime;

            _TaskRunner = new Thread(RunTasks);
            _TaskRunner.Start();
        }

        public void Add(IExecutionItem executionItem)
        {
            var count = Interlocked.Increment(ref _Count);
            if (count > _Queue.Length) Resize(count);
            var taskRunner = new TaskRunner(executionItem, this);
            taskRunner.Run();
        }

        private void Resize(int count)
        {
            _UpdateQueue.EnterWriteLock();
            try
            {
                var queue = _Queue;
                if (count < queue.Length) return;

                _Shift++;
                var newQueue = new TaskRunner[checked(0x1 << _Shift)];

                queue.CopyTo(newQueue, 0);
                _Queue = newQueue;
                if (_Index > 0)
                    _Index &= _Mask;
                _Mask = ((0x1L << _Shift) - 1L) | long.MinValue;
            }
            finally { _UpdateQueue.ExitWriteLock(); }
        }

        private DateTime OnFinished(TaskRunner taskRunner)
        {
            _UpdateQueue.EnterReadLock();
            try
            {
                long index = Interlocked.Increment(ref _Index) & _Mask;
                var result = DateTime.Now;
                _Queue[index] = taskRunner;
                return result;
            }
            finally
            {
                _UpdateQueue.ExitReadLock();
                _RunnerMRE.Set();
            }
        }

#if TEST_TIME
        private (RunTaskState, int) RunTasks(ref int currentIndex)
#else
        private RunTaskState RunTasks(ref int currentIndex)
#endif
        {
#if TEST_TIME
            int countStarted = 0;
#endif
            _UpdateQueue.EnterReadLock();
            try
            {
                int startIndex = currentIndex;
                while (true)
                {
                    for (int i = startIndex; i < _Queue.Length; i++)
                    {
                        var runner = _Queue[i];

                        if (runner == null)
                        {
                            var currentPasteIndex = _Index;
                            if (i >= (currentPasteIndex & _Mask))
                                _RunnerMRE.Reset();

                            currentIndex = i;
#if TEST_TIME
                            return (RunTaskState.WaitForNext(), countStarted);
#else
                            return RunTaskState.WaitForNext();
#endif
                        }

                        var lastFinishedTime = runner.LastFinishedTime;
                        var delta = lastFinishedTime + SleepTime - DateTime.Now;
                        if (delta < TimeSpan.FromSeconds(0.1))
                        {
                            _Queue[i] = null;
                            runner.Run();
#if TEST_TIME
                            countStarted++;
#endif
                        }
                        else
                        {
                            currentIndex = i;
#if TEST_TIME
                            return (RunTaskState.ToSleep(delta), countStarted);
#else
                            return RunTaskState.ToSleep(delta);
#endif
                        }
                    }

                    startIndex = 0;
                }
            }
            finally { _UpdateQueue.ExitReadLock(); }
        }

        private void RunTasks()
        {
#if TEST_TIME
            long fullStartedsTasks = 0;
            double fullTimeForStart = 0;

            Stopwatch stopwatch = new Stopwatch();
#endif

            int currentIndex = 0;

            while (true)
            {

#if TEST_TIME
                stopwatch.Reset();
                stopwatch.Start();
                (var state, var countStarted) = RunTasks(ref currentIndex);
                stopwatch.Stop();
                fullStartedsTasks += countStarted;
                fullTimeForStart += stopwatch.ElapsedMilliseconds;

                if (countStarted > 0)
                    Console.WriteLine($"Started {countStarted}\t/({stopwatch.Elapsed})\t=({stopwatch.ElapsedMilliseconds / (double)countStarted})\t|{fullTimeForStart / fullStartedsTasks}");
#else
                var state = RunTasks(ref currentIndex);
#endif

                if (state.IsWatingForNext)
                    _RunnerMRE.Wait();
                else
                    Thread.Sleep(state.TotalMiliseconds);
            }
        }
    }

    class Program
    {
        private sealed class EItem : IExecutionItem
        {
            private readonly int _TaskId;
            private readonly double _WaitTime;

            public EItem(int id, double waitTime)
            {
                _WaitTime = waitTime;
                _TaskId = id;
            }

            public async Task Run()
            {
                //Console.WriteLine($"Started task {_TaskId}");
                await Task.Delay(TimeSpan.FromSeconds(_WaitTime));
                //Console.WriteLine($"Finished task {_TaskId}");
            }
        }

        static void Main(string[] args)
        {
            var schedule = new Scheduler(TimeSpan.FromSeconds(2));

            Random random = new Random();
            for (int i = 0; i < 100_000; i++)
            {
                if (i % 1024 == 0) Console.WriteLine($"Added {i}");
                schedule.Add(new EItem(i, random.NextDouble()));
            }

            Console.WriteLine("Finish main thread");
            Console.ReadLine();
        }
    }
}
