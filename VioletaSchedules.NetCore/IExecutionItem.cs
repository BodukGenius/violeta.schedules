﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VioletaSchedules.NetCore
{
    public interface IExecutionItem
    {
        Task Run();
    }
}
