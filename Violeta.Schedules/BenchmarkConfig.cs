﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Violeta.Schedules
{
    class BenchmarkConfig : ManualConfig
    {
        public BenchmarkConfig()
        {
            ArtifactsPath = $"..\\..\\Benchmarks.Results\\";
            Add(Job.RyuJitX64);
            Add(MarkdownExporter.Default, MarkdownExporter.GitHub, MarkdownExporter.Atlassian);
        }
    }
}
