﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Violeta.Schedules
{
    public sealed class ExecuctionItemInfo
    {
        private readonly IExecutionItem _ExecutionItem;
        private readonly Action<Task> _ContinueWithAction;
        private readonly Action _OnFinishedTask;

        private TaskAwaiter _TaskAwaiter;
        private DateTime _LastFinishedTime;
        private WaitHandle _WaitHandle;

        public ExecuctionItemInfo(IExecutionItem executionItem)
        {
            _ExecutionItem = executionItem ?? throw new ArgumentNullException(nameof(executionItem));
            _ContinueWithAction = ContinueWith;
            _OnFinishedTask = OnFinishedTask;
        }

        public void Clear()
        {
            _TaskAwaiter = default(TaskAwaiter);
            _WaitHandle = null;
        }

        public async Task RunAsync()
        {
            await _ExecutionItem.Run();
            OnFinishedTask();
        }

        public Task RunUsageContinueWith() => _ExecutionItem.Run().ContinueWith(_ContinueWithAction);
        public Task RunUsageAwaiter()
        {
            var task = _ExecutionItem.Run();
            var awaiter = task.GetAwaiter();
            awaiter.OnCompleted(_OnFinishedTask);
            _TaskAwaiter = awaiter;
            return task;
        }

        public void RunUsageWaitHandle()
        {
            var task = _ExecutionItem.Run();
            _WaitHandle = ((IAsyncResult)task).AsyncWaitHandle;
        }

        private void ContinueWith(Task task)
        {
            task.Dispose();
            OnFinishedTask();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public void OnFinishedTask()
        {
            //_LastFinishedTime = DateTime.Now;
        }
    }
}
