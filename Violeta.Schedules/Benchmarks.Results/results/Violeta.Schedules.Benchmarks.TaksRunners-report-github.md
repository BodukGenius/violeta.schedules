``` ini

BenchmarkDotNet=v0.11.1, OS=Windows 7 SP1 (6.1.7601.0)
Intel Core i5-6440HQ CPU 2.60GHz (Skylake), 1 CPU, 4 logical and 4 physical cores
Frequency=2531318 Hz, Resolution=395.0511 ns, Timer=TSC
  [Host]    : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 32bit LegacyJIT-v4.7.3190.0
  RyuJitX64 : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3190.0

Job=RyuJitX64  Jit=RyuJit  Platform=X64  
InvocationCount=1  UnrollFactor=1  

```
|               Method |       Mean |    Error |    StdDev |     Median | Scaled | ScaledSD |  Allocated |
|--------------------- |-----------:|---------:|----------:|-----------:|-------:|---------:|-----------:|
|             RunAsync | 1,748.2 us | 87.79 us | 258.86 us | 1,845.5 us |   2.20 |     0.33 | 1000.05 KB |
| RunUsageContinueWith |   950.4 us | 29.78 us |  87.33 us |   914.8 us |   1.20 |     0.12 |  640.05 KB |
|   RunUsageWaitHandle | 4,252.6 us | 52.79 us |  49.38 us | 4,241.1 us |   5.36 |     0.21 |     848 KB |
|      RunUsageAwaiter |   795.2 us | 15.75 us |  29.96 us |   793.5 us |   1.00 |     0.00 |  400.05 KB |
