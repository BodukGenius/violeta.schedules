﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;

namespace Violeta.Schedules.Benchmarks
{
    [Config(typeof(BenchmarkConfig))]
    [MemoryDiagnoser]
    public class TaksRunners
    {
        private sealed class ExecutionItem : IExecutionItem
        {
            public Task Run() => Task.Run(() => Thread.Yield());
        }

        private const int _Repeats = 3;
        private const int _Count = 1024;
        private readonly ExecuctionItemInfo[] _ExecuctionItemInfoes 
            = new ExecuctionItemInfo[_Count];

        public TaksRunners()
        {
            for (int i = 0; i < _ExecuctionItemInfoes.Length; i++)
                _ExecuctionItemInfoes[i] = new ExecuctionItemInfo(new ExecutionItem());
        }


        [IterationCleanup]
        public void IterationCleanup()
        {
            for (int i = 0; i < _ExecuctionItemInfoes.Length; i++)
                _ExecuctionItemInfoes[i].Clear();
        }

        [Benchmark]
        public void RunAsync()
        {
            Task[] tasks = new Task[_Count ];
            for(int ir = 0; ir < _Repeats; ir ++)
                for (int i = 0; i < _ExecuctionItemInfoes.Length; i++)
                    tasks[i] = _ExecuctionItemInfoes[i].RunAsync();
        }

        [Benchmark]
        public void RunUsageContinueWith()
        {
            Task[] tasks = new Task[_Count];
            for (int ir = 0; ir < _Repeats; ir++)
                for (int i = 0; i < _ExecuctionItemInfoes.Length; i++)
                    tasks[i] = _ExecuctionItemInfoes[i].RunUsageContinueWith();
        }

        [Benchmark]
        public void RunUsageWaitHandle()
        {
            for (int ir = 0; ir < _Repeats; ir++)
                for (int i = 0; i < _ExecuctionItemInfoes.Length; i++)
                    _ExecuctionItemInfoes[i].RunUsageWaitHandle();
        }

        [Benchmark(Baseline = true)]
        public void RunUsageAwaiter()
        {
            Task[] tasks = new Task[_Count];
            for (int ir = 0; ir < _Repeats; ir++)
                for (int i = 0; i < _ExecuctionItemInfoes.Length; i++)
                    tasks[i] = _ExecuctionItemInfoes[i].RunUsageAwaiter();
        }
    }
}
